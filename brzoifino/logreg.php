<?php
include('includes/button.php');
include('includes/header.php');
include('includes/connect.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dodaj recept</title>
</head>

<body>
  <div class="container">
    <p>Dobrodošli nazad :) prijavite se i podijelite recept s nama!:
            <a href="login.php" class="btn btn-info btn-lg">
              <span class="glyphicon glyphicon-log-in"></span> Log in
            </a>
          </p> 
          <br><br><br>
          
          <p>Ako još niste član, što čekate, u par koraka registrirajte se na našu stranicu<br/>i podijelite svoj recept:
            <a href="register.php" class="btn btn-info btn-lg">
              <span class="glyphicon glyphicon-user"></span> User 
            </a>
          </p>       
  </div>
</body>
</html>