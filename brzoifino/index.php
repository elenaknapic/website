<?php
session_start();
error_reporting(0);
$spoj = mysqli_connect("localhost", "root", "", "recepti");
if(mysqli_connect_errno()){
echo "Spoj na bazu nije uspio!!".mysqli_connect_error();
}
function make_query($spoj){
$sql = "SELECT * FROM glavnojelo ORDER BY ID ASC";


$result=mysqli_query($spoj, $sql);
return $result;
}
function make_slide_indicators($spoj)
{
 $output = ''; 
 $count = 0;
 $result = make_query($spoj);
 while($row = mysqli_fetch_array($result))
 {
  if($count == 0)
  {
   $output .= '
   <li data-target="#dynamic_slide_show" data-slide-to="'.$count.'" class="active"></li>
   ';
  }
  else
  {
   $output .= '
   <li data-target="#dynamic_slide_show" data-slide-to="'.$count.'"></li>
   ';
  }
  $count = $count + 1;
 }
 return $output;
}

function make_slides($spoj)
{
 $output = '';
 $count = 0;
 $result = make_query($spoj);
 while($row = mysqli_fetch_array($result))
 {
  if($count == 0)
  {
   $output .= '<div class="item active">';
  }
  else
  {
   $output .= '<div class="item">';
  }
  $output .= '
   <img class="d-block w-100 h-50" src="Images/'.$row["img"].'" alt="'.$row["imejela"].' " />
   <div class="carousel-caption">
    <h3>'.$row["imejela"].'</h3>
    <a  href="glavnojelo.php">Pogledaj</a>
   </div>
  </div>
  ';
  $count = $count + 1;
 }
 return $output;
}

?>


<!DOCTYPE html>
<html lang="en">
   <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link rel = "stylesheet"type = "text/css" href = "myStyle.css" />
   <link rel="stylesheet" href="fancybox/jquery.fancybox.css">
   <script src="js/jquery.min.js"></script>
   <script src="fancybox/jquery.fancybox.js"></script>
<script>
$("[data-fancybox]").fancybox();
</script>
<style>
   .dynamic_slide_show {
   height: 100px;
   overflow: hidden;
   width: 50%;   
   }
   .gallery img {
    width: 20%;
    height: auto;
    border-radius: 5px;
    cursor: pointer;
    transition: .3s;
}
</style>
   

</head>
<?php
	
	include('includes/header.php');
?>
<body>

<h1>Dobrodošli</h1>

<div id="dynamic_slide_show" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
    <?php echo make_slide_indicators($spoj); ?>
    </ol>

    <div class="carousel-inner">
     <?php echo make_slides($spoj); ?>
    </div>
    <a class="left carousel-control" href="#dynamic_slide_show" data-slide="prev">
     <span class="glyphicon glyphicon-chevron-left"></span>
     <span class="sr-only">Previous</span>
    </a>

    <a class="right carousel-control" href="#dynamic_slide_show" data-slide="next">
     <span class="glyphicon glyphicon-chevron-right"></span>
     <span class="sr-only">Next</span>
    </a>

</div>

<div class="gallery">
    <?php
    // Include database configuration file
    include('Includes/connect.php') ;
    
    // Retrieve images from the database
    $query = $spoj->query("SELECT * FROM deserti WHERE status = 1 ORDER BY uploaded_on DESC");
    
    if($query->num_rows > 0){
        while($row = $query->fetch_assoc()){
            $imageURL = 'Images/'.$row["file_name"];
            $imageURL = 'Images/'.$row["file_name"];
    ?>
        <a href="<?php echo $imageURL; ?>" data-fancybox="gallery" data-caption="<?php echo $row["title"]; ?>" >
            <img src="<?php echo $imageURL; ?>" alt="" />
        </a>
    <?php }
    } ?>
</div>

</body>

<?php include('templates/footer.php'); ?>

