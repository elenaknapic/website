
<!DOCTYPE html>
<?php
include('includes/button.php');
include('includes/header.php');

?>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel = "stylesheet" type = "text/css" href = "myStyle.css" />
        <title><?php echo $title; ?></title>
        <style>
            img.center {
    display: block;
    margin-left: auto;
    margin-right: auto; }

            #myImg {
  display: block;
    margin-left: auto;
    margin-right: auto;
border-radius: 5px;
cursor: pointer;
transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}


#myImg2 {

border-radius: 5px;
cursor: pointer;
transition: 0.3s;
}

#myImg2:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
display: none; 
position: fixed; 
z-index: 1; 
padding-top: 100px; 
left: 0;
top: 0;
width: 100%; 
height: 100%; 
overflow: auto; 
background-color: rgb(0,0,0); 
background-color: rgba(0,0,0,0.9); 
}

/* Modal Content (image) */
.modal-content {
margin: auto;
display: block;
width: 80%;
max-width: 700px;
}

/* Caption of Modal Image */
#caption {
margin: auto;
display: block;
width: 80%;
max-width: 700px;
text-align: center;
color: #ccc;
padding: 10px 0;
height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
-webkit-animation-name: zoom;
-webkit-animation-duration: 0.6s;
animation-name: zoom;
animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
from {-webkit-transform:scale(0)} 
to {-webkit-transform:scale(1)}
}

@keyframes zoom {
from {transform:scale(0)} 
to {transform:scale(1)}
}

/* The Close Button */
.close {
position: absolute;
top: 15px;
right: 35px;
color: #f1f1f1;
font-size: 40px;
font-weight: bold;
transition: 0.3s;
}

.close:hover,
.close:focus {
color: #bbb;
text-decoration: none;
cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
.modal-content {
  width: 100%;
}
}
    .center {
    margin-left: auto;
    margin-right: auto;
    display: block
}

    </style>
            

    </head>
  <body>


  <div class ="container">
      <h1>20 savjeta za kuhanje koje vam je mama možda zaboravila prenijeti</h1>
      <h3>Ponavljanje ja majka mudrosti, a ono će vam biti od velike koristi ako želite usavršiti svoje kuharske vještine.
          <br> No na tome putu pomoći će vam i ovi kuharski trikovi.</h3>
        <p>1. Držite crveni luk u hladnjaku: lakše ćete ga iscjeckati i manje će vam suziti oči.<br/>

2. Želite li ispeći kremastija jaja na kajganu dodajte im mrvicu vrhnja za kuhanje.<br/>

3. Zamrznite ljetno bobičasto voće na sljedeći način: rasporedite ga po protvanu za pečenje i tako stavite u led.
 Nakon zamrzavanja premjestite ga u vrećice za pohranu. Umjesto stopljenih gruda imat ćete odvojene plodove.<br/>

4. Ne znate prepoznati al dente špagete? Pažljivo izvadite jedan rezanac iz kipuće vode, pustite da voda istekne iz njega pa ga bacite na okomito postavljen keramički tanjur.
 Ako ostane zalijepljen – vrijeme je za gašenje štednjaka! Ako pak sklizne s površine nastavite kuhati.<br/>

5. Preko dizanog tjesta stavite mokru krpu, pa se isto neće skoriti i tako upropastiti pečenje kruha, peciva i drugih pekarskih slastica.<br/>

6. Ulje za prženje dodajte na prethodno zagrijanu tavu, i tako spriječite ljepljenje namirnica za dno.<br/>

<img style="width:50%" id="myImg" class="imgCenter" src="Images/ulje.jpg" > <br>

7. Želite izvući svu vlagu iz neke namirnice, a ne želite kupovati profesionalni dehidrator? Stavite je u pećnicu na najnižu moguću temperaturu, a vrata pećnice ostavite priškrinuta uz pomoć drvene kuhače.<br/>

8. Za desert vam treba samo pola jabuke? Polovinu koju ne planirate iskoristiti pošpricajte limunovim sokom i tako spriječite tamnjenje ploda.<br/>

9. Namažite zaleđeno meso s malo octa, kako bi ubrzali proces odmrzavanja, ali i omekšali meso prije termičke obrade.<br/>

10. Produljite svježinu i spriječite prijevremeno kvarenje mlijeka na sljedeći način: ubacite prstohvat soli u mlijeko nakon što prvi put otvorite bocu.<br/>

11. Glazuru ćete lakše ravnomjerno razmazati preko torte ako nož nakratko namočite u toploj vodi, a onda se bacite na ukrašavanje.<br/>

12. Domaći sladoled brže će se zalediti u aluminijskoj posudi.<br/>
<img style="width:50%" id="myImg" class="imgCenter" src="Images/sladoled.jpg" > <br>

13. Stvrdnjuje li vam se sadržaj soljenke? Stavite u posudicu par zrna nekuhane riže koji će popiti svu vlagu.<br/>

14. U juhu ste stavili previše soli? U juhu stavite sirovi krumpir i pustite da se kuha dok ne 'popije' sav višak ovog začina.<br/>

15. Salatu ćete ravnomjernije začiniti ako sve sastojke dresinga prvotno promiješajte u staklenoj zdjelici s poklopcem, a tek onda prelijete preko povrća.<br/>

16. Svježoj ribi ćete lakše odstraniti ljuske, ako ju cijelu uronite u kipuću vodu na jednu minutu, pa onda nastavite uobičajenim putem.<br/>

17. Dodajte prstohvat soli u kipuću vodu prije nego što stavite kuhati jaja i tako spriječite eventualno pucanje opne. (Jaja nemojte 'bacati' u vodu, već ih nježno položite u kipuću vodu uz pomoć žlice.)<br/>

18. Krišku jabuke položite na stvrdnutu grudu smeđeg šećera jer će ju ona 'opustiti'.<br/>

19. Ne ostavljajte začine u blizini štednjaka jer gube svoj okus na vlažnim, svjetlim i toplim mjestima.<br/>

20. Ne podnosite miris i okus dinstanog češnjaka? Ispecite ga u ljusci u pećnici, pa onda kremastu strukturu dodajte umacima, mesu i drugim jelima.<br/>
</p>

  </div>

  <div id="myModal" class="modal">
        <span class="close">&times;</span>
        <img id="modal-img" class="modal-content"  src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQS2ol73JZj6-IqypxPZXYS3rRiPwKteoD8vezk9QsRdkjt3jEn&usqp=CAU">
        <div id="caption"></div>
      </div>
    
    <div class="footer">
        <p>Web stranica izrađena za vrijeme pohađanja <a href="https://www.ferit.unios.hr/">Fakulteta elektrotehnike, računarstva i informacijskih tehnologija u Osijeku</a></p>
     <p> u sklopu kolegija <a href="https://loomen.carnet.hr/course/view.php?id=3626">Multimedijska tehnika </a></p>
    </div>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
// var img = document.getElementById("myImg");
var modalImg = document.getElementById("modal-img");
var captionText = document.getElementById("caption");
// img.onclick = function(){
//   modal.style.display = "block";
//   modalImg.src = this.src;
//   captionText.innerHTML = this.alt;
// }


document.addEventListener("click", (e) => {
  const elem = e.target;
  if (elem.id==="myImg") {
    modal.style.display = "block";
    modalImg.src = elem.dataset.biggerSrc || elem.src;
    captionText.innerHTML = elem.alt; 
  }
})

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

</script>
  <?php include('includes/footer.php'); ?>
  </html>
 